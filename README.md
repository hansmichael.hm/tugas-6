# Hans' Status

**PPW Tugas 6 Project**


## Author

* Hans Michael Nabasa Pasaribu - 1806235662

## Herokuapp Link

[Hans' Status](https://statushans.herokuapp.com) 

## Project Details

Web ini berfungsi sebagai tempat saya membuat status keadaan saya saat ini yang kemudian ditampilkan kepada semua orang yang mengakses web saya

## Pipelines Status

[![pipeline status](https://gitlab.com/hansmichael.hm/tugas-6/badges/master/pipeline.svg)](https://gitlab.com/hansmichael.hm/tugas-6/commits/master)

## Coverage Status
[![coverage report](https://gitlab.com/hansmichael.hm/tugas-6/badges/master/coverage.svg)](https://gitlab.com/hansmichael.hm/tugas-6/commits/master)