from django import forms
from . import models

class Status_Form(forms.Form):
    status_message = forms.CharField(label='Enter Status Here ', required=True,
                             max_length=300,widget=forms.TextInput())
    
class Meta:
    model = models.Fill
    fields = ('status')

