from django.urls import path
from . import views

app_name = 'statusApp'
urlpatterns = [
    path('',views.index, name = 'index'),
]
