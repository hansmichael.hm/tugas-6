from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import Status_Form
from . import models

def index(request):
    status_f= Status_Form()
    if request.method == 'POST':
        status_f = Status_Form(request.POST)
        if status_f.is_valid():
            status = models.Fill(status=status_f.cleaned_data['status_message'])
            status.save()
    status = models.Fill.objects.all()
    response={'status_f':status_f,'status':status}
    return render(request, 'index.html', response)

