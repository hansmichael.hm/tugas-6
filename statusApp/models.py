from django.db import models
from django.utils import timezone
from datetime import datetime

class Fill(models.Model):
    status = models.CharField(max_length=300)
    published_date = models.DateTimeField(default=timezone.now)
